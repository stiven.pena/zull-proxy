package co.com.soaint.service;

import co.com.soaint.commons.domains.entities.AuditWS;

import java.util.Optional;

public interface IGestionAuditWS {

    Optional<AuditWS> createAuditoria(AuditWS auditWS);
}
