package co.com.soaint.repository.impl;

import co.com.soaint.commons.domains.entities.AuditWS;

import java.util.Optional;


public interface AuditWsRepositoryFacade{

    Optional<AuditWS> createAuditoria(AuditWS auditWS);

}
