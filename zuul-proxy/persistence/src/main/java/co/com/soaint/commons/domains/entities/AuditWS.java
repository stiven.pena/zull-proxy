package co.com.soaint.commons.domains.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(name = "audit_ws")
public class AuditWS {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "endopoint")
    private String endopoint;
    @Column(name = "ip")
    private String ip;
    @Column(name = "port")
    private String port;
    @Column(name = "method")
    private String method;
    @Column(name = "status_code")
    private Integer statusCode;
    @Column(name = "request")
    private String request;
    @Column(name = "response")
    private String response;
    @Column(name = "created_in")
    private Date createdIn;
}
