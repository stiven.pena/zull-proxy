package co.com.soaint.repository;

import co.com.soaint.commons.domains.entities.AuditWS;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAuditWS extends JpaRepository<AuditWS, Integer> {

}
