package co.com.soaint.service.impl;

import co.com.soaint.repository.impl.AuditWsRepositoryFacade;
import co.com.soaint.commons.domains.entities.AuditWS;
import co.com.soaint.service.IGestionAuditWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GestionAuditImpl implements IGestionAuditWS {

    private AuditWsRepositoryFacade repositoryFacade;

    @Autowired
    public GestionAuditImpl(AuditWsRepositoryFacade repositoryFacade) {
        this.repositoryFacade = repositoryFacade;
    }

    @Override
    public Optional<AuditWS> createAuditoria(AuditWS auditWS) {
        return repositoryFacade.createAuditoria(auditWS);
    }
}
