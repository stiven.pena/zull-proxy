package co.com.soaint.repository.impl;

import co.com.soaint.repository.IAuditWS;
import co.com.soaint.commons.domains.entities.AuditWS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuditWsRepositoryImpl implements AuditWsRepositoryFacade {

    private final IAuditWS repository;

    @Autowired
    public AuditWsRepositoryImpl(IAuditWS repository) {
        this.repository = repository;
    }

    @Override
    public Optional<AuditWS> createAuditoria(AuditWS auditWS) {
        return Optional.ofNullable(repository.saveAndFlush(auditWS));
    }
}
