package co.com.soaint.filter;

import co.com.soaint.commons.domains.entities.AuditWS;
import co.com.soaint.service.IGestionAuditWS;
import com.google.common.io.CharStreams;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.stream.Collectors;

public class Filter extends ZuulFilter {

    public AuditWS auditWS;

    @Autowired
    private IGestionAuditWS gestionAuditWS;
    private static Logger log = LoggerFactory.getLogger(Filter.class);

    @Override
    public Object run() throws ZuulException {
        auditWS = new AuditWS();
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletResponse res = ctx.getResponse();
        HttpServletRequest req = ctx.getRequest();
        Long dateM = new Date().getTime();
        auditWS.setEndopoint(req.getRequestURI());
        auditWS.setIp(req.getRemoteAddr());
        auditWS.setPort(String.valueOf(ctx.getRouteHost().getPort()));
        auditWS.setMethod(req.getMethod());
        if ("POST".equalsIgnoreCase(req.getMethod()))
        {
            String request = null;
            try {
                request = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            auditWS.setRequest(request);
        }
        auditWS.setStatusCode(res.getStatus());
        try (final InputStream responseDataStream = ctx.getResponseDataStream()) {
            final String responseData = CharStreams.toString(new InputStreamReader(responseDataStream, "UTF-8"));
            ctx.setResponseBody(responseData);
            auditWS.setResponse(ctx.getResponseBody());
        } catch (IOException e) {
            log.error("Error reading body " + e);
        }
        auditWS.setCreatedIn(new Date(dateM));
        gestionAuditWS.createAuditoria(auditWS);

        return null;
    }

    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

}
