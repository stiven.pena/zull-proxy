package co.com.soaint.example.restservice.service;

import co.com.soaint.example.restservice.model.OperationDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CalculatorService {



    public ResponseEntity<OperationDTO> sum( OperationDTO dto ){
        dto.setResult(dto.getFirtValue()+dto.getSecondValue());
        return new ResponseEntity<>(dto,HttpStatus.OK);

    }
}
