package co.com.soaint.example.restservice.controller;


import co.com.soaint.example.restservice.model.OperationDTO;
import co.com.soaint.example.restservice.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class CalculatorController {

    @Autowired
    CalculatorService service;

    @GetMapping(value = "/getExample",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getExaple(){

        return new ResponseEntity<>("this is a example get resoruce", HttpStatus.ACCEPTED);

    }


    @PostMapping("/sum")
    public ResponseEntity<?> sumResult(@RequestBody OperationDTO operationDTO){
        return  service.sum(operationDTO);
    }

}
